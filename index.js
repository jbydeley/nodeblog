var App = require('./api/app')
  , Database = require('./api/database')
  , Routes = require('./api/routes');

var config = require('./config/config');

var app = new App(config);
var database = new Database(config);
var routes = new Routes(app);

if (!module.parent) app.listen(process.env.PORT || 3000);

module.exports = app;