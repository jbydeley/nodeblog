var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

var userSchema = new Schema({
	username: {
		type: String,
		required: true,
		unique: true
	},
	password: {
		type: String,
		required: true
	},
	role: {
		type: Number,
		required: true,
		default: 1
	}
});

module.exports = mongoose.model('User', userSchema);

/*
Roles:
1 = Writer
2 = Editor
3 = Admin ?
*/