var mongoose = require('mongoose')
  , Schema = mongoose.Schema
  , ObjectId = Schema.ObjectId;

var postSchema = new Schema({
	author: {
		type: ObjectId,
		ref: 'User',
		required: true
	},
	title: {
		type: String,
		required: true
	},
	body: {
		type: String,
		required: true
	},
	createdAt: {
		type: Date,
		default: new Date
	}
});

module.exports = mongoose.model('Post', postSchema);