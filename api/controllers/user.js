var User = require('../models/user.js');

exports.create = function (req, res) {
   new User(req.body)
      .save(function(err, user) {
         if (err) res.send(409);

         res.json(user);
      });
};

exports.read = function (req, res) {
   User.find(function (err, users) {
      res.json(users);
   });
};

exports.readById = function (req, res) {
   User.findById(req.params.id, function (err, user) {
      res.json(user);
   });
};

exports.remove = function (req, res, next) {
   User.findById(req.params.id, function (err, user) {
      user.remove()
      res.send(200);
   });
};