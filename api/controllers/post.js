var Post = require('../models/post');

exports.create = function (req, res) {
   new Post(req.body)
      .save(function(err, post) {
         if (err) res.send(409, err);
         Post.findById(post._id).populate('author').exec(function(err, retPost) {
            res.json(retPost);   
         });
         
      });
};

exports.read = function (req, res) {
   Post.find({}, '-__v').populate('author', '_id username').exec(function (err, posts) {
      res.json(posts);
   });
};

exports.readById = function (req, res) {
   Post.findById(req.params.id, '-__v').populate('author', '_id username').exec(function (err, post) {
      res.json(post);
   });
};

exports.remove = function (req, res, next) {
   Post.findById(req.params.id, function (err, post) {
      user.remove()
      res.send(200);
   });
};