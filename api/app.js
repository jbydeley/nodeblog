var express = require('express')
  , SessionStore = require('session-mongoose')(express)
  , app = express();

module.exports = function App (config) {
	app.configure(function() {
		app.use(express.urlencoded());
		app.use(express.json());
		app.use(express.methodOverride());
		app.use(express.cookieParser());
		app.use(
		  express.session({
		    store: new SessionStore({
		    url: config[process.env.NODE_ENV].url,
		    interval: config[process.env.NODE_ENV].interval
		  }),
		  cookie: { maxAge: config[process.env.NODE_ENV].maxAge },
		  secret: 'my secret'
		}))

		app.use(express.static(__dirname + '/../public'));

		app.engine('.html', require('ejs').__express);

		app.set('views', __dirname + "/../views");
		app.set('view engine', 'html');

		app.use(app.router);
	});

	return app;
}