
module.exports = function Routes(app) {

	/* User API */
	var user = require('./controllers/user');

	app.get('/users', user.read);
	app.get('/users/:id', user.readById);
	app.post('/users', user.create);
	app.delete('/users/:id', user.remove);

	/* Post API */
	var post = require('./controllers/post');

	app.get('/posts', post.read);
	app.get('/posts/:id', post.readById);
	app.post('/posts', post.create);
	app.delete('/posts/:id', post.remove);
}