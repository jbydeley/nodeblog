var mongoose = require('mongoose');

module.exports = function Database(config) {
	mongoose.connect(config[process.env.NODE_ENV].url);

	return mongoose;
}