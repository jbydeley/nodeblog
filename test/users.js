var request = require('supertest')
  , expect = require('chai').expect
  , app = require('../index')
  , User = require('../api/models/user');

request = request(app);



describe('User API', function() {
	var valid_user = {username: "Jared", password: "password"};
	var invalid_user = {password: "password"};

	beforeEach(function(done) {
		User.find({}, function (err, users) {
			users.forEach(function(user) {
				user.remove();
			});
			done();
		});
	});

	describe('GET /users', function() {
		
		it('should return a list of users', function(done) {

			new User(valid_user).save(function(err, res) {
				if (err) throw err;
			});

			request
				.get('/users')
				.expect('Content-Type', /json/)
				.expect(200)
				.end(function(err, res) {
					if(err) throw err;

					expect(res.body.length).to.equal(1);
					done();
				});
		});
	});

	describe('GET /users/:id', function() {
		
		var user;

		beforeEach(function (done) {
			new User(valid_user).save(function(err, res) {
				if (err) throw err;
				user = res;
				done();
			});
		});

		it('should return a user', function(done) {
			request
				.get('/users/' + user._id)
				.expect('Content-Type', /json/)
				.expect(200)
				.end(function(err, res) {
					if(err) throw err;

					expect(res.body.username).to.equal(valid_user.username);
					expect(res.body.password).to.equal(valid_user.password);
					done();
				});
		});
	});

	describe('POST /users', function() {

		it('should save a user and return that user', function(done) {
			request
				.post('/users')
				.send(valid_user)
				.expect('Content-Type', /json/)
				.expect(200)
				.end(function(err, res) {
					if(err) throw err;

					expect(res.body.username).to.equal(valid_user.username);
					expect(res.body.password).to.equal(valid_user.password);
					expect(res.body._id).to.not.be.null;
					done();
				});
		});

		it('should fail to save a user and return 409 when username already taken', function(done) {
			new User(valid_user).save(function(err, res) {
				if (err) throw err;
			});

			request
				.post('/users')
				.send(valid_user)
				.expect('Content-Type', /plain/)
				.expect(409, done);
		});

		it('should fail to save a user and return 409 when username too short', function(done) {
			request
				.post('/users')
				.send(invalid_user)
				.expect('Content-Type', /plain/)
				.expect(409, done);
		});
	});
});