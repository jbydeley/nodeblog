var request = require('supertest')
  , expect = require('chai').expect
  , app = require('../index')
  , User = require('../api/models/user')
  , Post = require('../api/models/post');

request = request(app);

describe('Post API', function() {

	before(function(done) {
		User.find({}, function (err, users) {
			users.forEach(function(user) {
				user.remove();
			});
		});

		Post.find({}, function (err, posts) {
			posts.forEach(function(user) {
				user.remove();
			});
			done();
		});
	});

	describe('GET /posts', function() {

		it('should return a list of posts', function(done) {
			request
				.get('/posts')
				.expect('Content-Type', /json/)
				.expect(200)
				.end(function(err, res) {
					if(err) throw err;

					expect(res.body.length).to.equal(0);
					done();
				});
		});
	});

	describe('POST /posts', function() {
		var writer = { username: 'Writer', password: 'password', role: 1 };

		before(function(done) {
			new User(writer).save(function(err, res) {
				if (err) throw err;

				writer = res;
				done();
			});
		});

		it('should save a post and return that post', function(done) {
			var post = { author: writer._id, title: 'Title', body: 'Body' };

			request
				.post('/posts')
				.send(post)
				.expect('Content-Type', /json/)
				.expect(200)
				.end(function(err, res) {
					if(err) throw err;

					expect(res.body.author._id).to.equal(String(writer._id));
					expect(res.body.author.username).to.equal(writer.username);
					// expect(res.body.author).to.not.have.property('password');
					expect(res.body.title).to.equal(post.title);
					expect(res.body._id).to.not.be.null;

					done();
				});
		});
	});
});