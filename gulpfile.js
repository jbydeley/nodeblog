var gulp = require('gulp')
  , gutil = require('gulp-util')
  , grep = require('gulp-grep-stream') //REMOVE
  , mocha = require('gulp-mocha')
  , watch = require('gulp-watch');

gulp.task('default', function() {
	process.env.NODE_ENV = 'development';
	
	gulp.src('test/*.js', {read: false})
		.pipe(watch())
		.pipe(mocha({reporter: 'spec'}))
		.on('error', function(err) {
	        if (!/tests? failed/.test(err.stack)) {
	            console.log(err.stack);
	        }
	    });
})